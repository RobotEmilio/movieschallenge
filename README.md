## Tv shows challenge

### Summary
This project consists in an Android app that shows a list of tv shows and movie titles to the user.
For each show, it will feature it's cover art, title, a brief description and their average score

The shows will be shown in a 'infinite-scroll' fashion

### Context
This project has been developed by Antonio Suárez Galán as part of a challenge 

### Description
When the user opens the app, it already tries to load the first results of the show.
As the user scrolls down, the app comunicates with The Movie API requesting batches of shows to load, in pages
of 20 elements. Since the app preloads new information before arriving at the end of the scroll, the user experience
should be that it does not have any interruption whatsoever.

In case the network connection falls or is disabled, the app will notify the user that it couldn't access the external API

### Architecture
This project has been developed following a Model-View-Viewmodel (MVVM) architecture <br />
![](art/ArchitectureDiagram.png) <br />

The choice of this architecture is due to the fact that Viewmodels are entities that are aware of
Android activities lifecycle, but are no created by them. This means that we can have a separate instance
from the view where to receive data asynchronously and communicate to the UI when it's ready

The different layers are also implemented following Clean architecture principles. So, each layer is isolated
from the others by using different interactors between them (e.g: Data sources are separated from business logic
using repositories, UI relies on viewModel to feed the information needed, etc.). Also each layer has its own 
entity model. This is done so they can be decoupled and to make each one have it's own single responsability

UI layer responsability is solely to show information and notify user interactions with the app <br/>
VM layer contains all data that has to be served to UI <br/>
Domain layer contains high-level business use cases (in this case, retrieving tv shows from TheMovieDatabase API). It also contains the interactors to Data layer<br/>
Data layer is responsible for managing the different external sources the app nourish from<br/>

The flow of data from Data Layer to VM Layer, in this app, is implemented using Rx-kotlin library.
This library allows that all data exchange to be based on asynchronous streams. This way, whenever the data is served
to the app, it will react and transform the data until it reaches VM, so it can be served to UI layer.    
This library also allows us to exchange the thread we are working in, so it allows for a more efficent solution

### Dependency Injection
![](art/DependencyInjection.png)<br/>


    
