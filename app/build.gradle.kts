import com.asuarezgalan.gradle.Libs
import com.asuarezgalan.gradle.TestingLibs

plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("android.extensions")
    kotlin("kapt")
}

android {
    compileSdkVersion(28)
    defaultConfig {
        applicationId = "com.asuarezgalan.movieschallenge"
        minSdkVersion(21)
        targetSdkVersion(28)
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        buildConfigField("String", "API_KEY", "\"NWQ5NjdjN2MzMzU3NjRmMzliMWVmYmU5YzVkZTk3NjA=\"")
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro")
        }
    }
}

dependencies {
    implementation(Libs.Kotlin)
    implementRetrofitDependencies()
    implementDaggerDependencies()
    implementAndroidDependencies()
    implementRxDependencies()
    implementation(Libs.Glide)
    implementation(Libs.LiveEvent)

    testImplementation(TestingLibs.JUnit)
    testImplementation(TestingLibs.MockK)
    androidTestImplementation(TestingLibs.Android.TestRunner)
    androidTestImplementation(TestingLibs.Android.Espresso)
}

fun DependencyHandlerScope.implementAndroidDependencies() {
    implementation(Libs.Android.LifecycleExtensions)
    implementation(Libs.Android.Core)
    implementation(Libs.Android.AppCompat)
    implementation(Libs.Android.ConstraintLayout)
    implementation(Libs.Android.NavigationFragment)
    implementation(Libs.Android.NavigationUi)
}

fun DependencyHandlerScope.implementDaggerDependencies() {
    implementation(Libs.Dagger.Dagger)
    kapt(Libs.Dagger.DaggerCompiler)
    implementation(Libs.Dagger.DaggerAndroid)
    kapt(Libs.Dagger.DaggerAndroidProcessor)
    implementation(Libs.Dagger.DaggerSupport)
}

fun DependencyHandlerScope.implementRetrofitDependencies() {
    implementation(Libs.Retrofit.Retrofit)
    implementation(Libs.Retrofit.RetrofitGsonConverter)
    implementation(Libs.Retrofit.HttpLoggingInterceptor)
    implementation(Libs.Retrofit.RetrofitRxAdapter)
}

fun DependencyHandlerScope.implementRxDependencies() {
    implementation(Libs.Rx.RxKotlin)
    implementation(Libs.Rx.RxAndroid)
}