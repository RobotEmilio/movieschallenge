package com.asuarezgalan.tvshowschallenge.rx

import androidx.lifecycle.LiveData
import com.asuarezgalan.tvshowschallenge.common.data.network.exceptions.HostNotFoundException
import com.asuarezgalan.tvshowschallenge.common.vm.UiAction
import com.asuarezgalan.tvshowschallenge.rx.applyLoadings
import com.asuarezgalan.tvshowschallenge.rx.mapErrors
import com.hadilq.liveevent.LiveEvent
import io.mockk.*
import io.reactivex.Single
import org.junit.Test

class RxExtensionsKtTest {

    @Test
    fun applyLoadingsTest() {
        val mockLiveData = mockk<LiveEvent<UiAction>>()

        Single.just("")
            .applyLoadings(mockLiveData)
            .test()
            .assertSubscribed()
            .assertTerminated()
        verify(exactly = 2) { mockLiveData.value = any<UiAction.Loading>() }
    }

    @Test
    fun mapErrorsTest_withFailure() {
        val throwableMock = mockk<Throwable>()
        val missingHostMock = mockk<HostNotFoundException>()
        val transformer = mockk<(Throwable) -> Throwable>()
        every { transformer.invoke(any<Throwable>()) } returns missingHostMock

        Single.error<Int> { throwableMock }
            .mapErrors(transformer)
            .test()
            .assertError(missingHostMock)
    }

    @Test
    fun mapErrorsTest_withSuccess() {
        val missingHostMock = mockk<HostNotFoundException>()
        val transformer = mockk<(Throwable) -> Throwable>()
        every { transformer.invoke(any<Throwable>()) } returns missingHostMock

        Single.just(1)
            .mapErrors(transformer)
            .test()
            .assertValueCount(1)
            .assertTerminated()

        verify { transformer.invoke(any<Throwable>()) wasNot Called }
    }
}