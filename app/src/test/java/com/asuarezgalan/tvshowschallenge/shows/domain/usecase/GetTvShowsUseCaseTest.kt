package com.asuarezgalan.tvshowschallenge.shows.domain.usecase

import com.asuarezgalan.tvshowschallenge.common.data.network.exceptions.HostNotFoundException
import com.asuarezgalan.tvshowschallenge.configuration.domain.ConfigurationRepository
import com.asuarezgalan.tvshowschallenge.configuration.domain.mocks.ConfigurationDomainMockEntitiesProvider
import com.asuarezgalan.tvshowschallenge.configuration.domain.model.Configuration
import com.asuarezgalan.tvshowschallenge.shows.domain.ShowsRepository
import com.asuarezgalan.tvshowschallenge.shows.domain.exceptions.MissingConfigurationException
import com.asuarezgalan.tvshowschallenge.shows.domain.mocks.ShowsDomainMockEntitiesProvider
import com.asuarezgalan.tvshowschallenge.shows.domain.model.ShowListPage
import io.mockk.every
import io.mockk.mockk
import io.reactivex.Single
import org.junit.Before
import org.junit.Test

class GetTvShowsUseCaseTest {

    private lateinit var showsStream: Single<ShowListPage>
    private lateinit var configStream: Single<Configuration>
    private val showsRepoMock: ShowsRepository = mockk()
    private val configurationRepoMock: ConfigurationRepository = mockk()
    private val subjectTest = GetTvShowsUseCase(showsRepoMock, configurationRepoMock)

    @Before
    fun setUp() {
        showsStream = Single.just(ShowsDomainMockEntitiesProvider.getShowListPage())
        every { showsRepoMock.retrieveShowsFromNetwork(any<Int>()) } returns showsStream

        configStream = Single.just(ConfigurationDomainMockEntitiesProvider.getConfiguration())
        every { configurationRepoMock.getConfiguration() } returns configStream
    }

    /**
     * Tests that usecase successfully merges entities
     */
    @Test
    fun execute_Success() {
        val expectedShow = ShowsDomainMockEntitiesProvider.getShow()
        val expectedConfig = ConfigurationDomainMockEntitiesProvider.getConfiguration()
        val expectedResult =
            expectedConfig.baseUrl + expectedConfig.listCoverImageSize + expectedShow.coverUrl

        val testSubscriber = subjectTest.execute(1).test()

        with(testSubscriber) {
            assertSubscribed()
            awaitTerminalEvent()
            assertValueCount(1)
            assertValue { page -> page.results.all { it.coverUrl == expectedResult } }
            assertComplete()
            assertTerminated()
        }

    }

    /**
     * Tests that usecase successfully merges entities, but with configuration imageSize null
     */
    @Test
    fun execute_SuccessNullImageSize() {
        configStream = Single.just(ConfigurationDomainMockEntitiesProvider.getConfiguration().copy(listCoverImageSize = null))
        every { configurationRepoMock.getConfiguration() } returns configStream

        val expectedShow = ShowsDomainMockEntitiesProvider.getShow()
        val expectedConfig = ConfigurationDomainMockEntitiesProvider.getConfiguration()
        val expectedResult =
            expectedConfig.baseUrl + "null" + expectedShow.coverUrl

        val testSubscriber = subjectTest.execute(1).test()

        with(testSubscriber) {
            assertSubscribed()
            awaitTerminalEvent()
            assertValueCount(1)
            assertValue { page -> page.results.all { it.coverUrl == expectedResult } }
            assertComplete()
            assertTerminated()
        }

    }

    /**
     * Tests that if shows stream fails, the zip will fail and will propagate incoming error
     */
    @Test
    fun execute_ShowsFailure() {
        showsStream = Single.error(HostNotFoundException())
        every { showsRepoMock.retrieveShowsFromNetwork(any<Int>()) } returns showsStream

        val testSubscriber = subjectTest.execute(1).test()

        with(testSubscriber) {
            assertSubscribed()
            awaitTerminalEvent()
            assertError(HostNotFoundException::class.java)
            assertTerminated()
        }
    }

    /**
     * Tests that if config stream fails, it will fail but mapping the
     * excpetion to MissingConfigurationException
     */
    @Test
    fun execute_ConfigurationFailure() {
        configStream = Single.error(HostNotFoundException())
        every { configurationRepoMock.getConfiguration() } returns configStream

        val testSubscriber = subjectTest.execute(1).test()

        with(testSubscriber) {
            assertSubscribed()
            awaitTerminalEvent()
            assertError(MissingConfigurationException::class.java)
            assertTerminated()
        }
    }

    /**
     * Tests that, in case that both streams fail, shows error propagation will prevail
     */
    @Test
    fun execute_BothFailure() {
        showsStream = Single.error(HostNotFoundException())
        every { showsRepoMock.retrieveShowsFromNetwork(any<Int>()) } returns showsStream

        configStream = Single.error(HostNotFoundException())
        every { configurationRepoMock.getConfiguration() } returns configStream

        val testSubscriber = subjectTest.execute(1).test()

        with(testSubscriber) {
            assertSubscribed()
            awaitTerminalEvent()
            assertError(HostNotFoundException::class.java)
            assertTerminated()
        }
    }


}