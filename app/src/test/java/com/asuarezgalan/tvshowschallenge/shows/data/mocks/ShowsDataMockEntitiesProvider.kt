package com.asuarezgalan.tvshowschallenge.shows.data.mocks

import com.asuarezgalan.tvshowschallenge.common.data.network.model.PaginatedResponse
import com.asuarezgalan.tvshowschallenge.shows.data.network.model.ShowListResponse

/**
 * Returns data objects to be used in tests (not mocks since they're plain data objects)
 */
object ShowsDataMockEntitiesProvider {

    fun getPaginatedResponse() : PaginatedResponse<ShowListResponse> =
        PaginatedResponse(1, 1000, 200, listOf(getShowListResponse()))

    fun getShowListResponse(): ShowListResponse =
        ShowListResponse(1, "title", "overview", "imageId", 5f)
}