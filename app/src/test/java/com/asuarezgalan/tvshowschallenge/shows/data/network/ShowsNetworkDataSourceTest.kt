package com.asuarezgalan.tvshowschallenge.shows.data.network

import android.accounts.NetworkErrorException
import android.util.Base64
import com.asuarezgalan.tvshowschallenge.common.data.network.model.PaginatedResponse
import com.asuarezgalan.tvshowschallenge.shows.data.network.model.ShowListResponse
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import io.mockk.verify
import io.reactivex.Single
import org.junit.Before
import org.junit.Test

class ShowsNetworkDataSourceTest {

    private lateinit var apiMock: ShowsTvApi
    private lateinit var subjectTest: ShowsNetworkDataSource

    @Before
    fun setUp() {
        mockkStatic(Base64::class)
        apiMock = mockk<ShowsTvApi>()

        every { Base64.decode(any<String>(), any()) } returns "Key".toByteArray()

        subjectTest = ShowsNetworkDataSource(apiMock)
    }

    /**
     * Tests that network data source will succesfully return PaginatedResponse items
     */
    @Test
    fun getTvShows_Success() {
        val response = mockk<PaginatedResponse<ShowListResponse>>()
        every { apiMock.getTvShowsList(any<String>(), any()) } answers { Single.just(response) }

        val result = subjectTest.getTvShows(0)

        verify(exactly = 1) { apiMock.getTvShowsList(any<String>(), any<Int>()) }

        result.test()
            .assertSubscribed()
            .assertValueCount(1)
            .assertComplete()
            .assertTerminated()
    }

    /**
     * Tests that network data source will propagate errors caused by API
     */
    @Test
    fun getTvShows_Failure() {
        every { apiMock.getTvShowsList(any<String>(), any()) } answers {
            Single.error(NetworkErrorException())
        }

        val result = subjectTest.getTvShows(0)

        verify(exactly = 1) { apiMock.getTvShowsList(any<String>(), any<Int>()) }

        result.test()
            .assertSubscribed()
            .assertError(NetworkErrorException::class.java)
            .assertTerminated()
    }
}