package com.asuarezgalan.tvshowschallenge.shows.data

import com.asuarezgalan.tvshowschallenge.common.data.network.exceptions.HostNotFoundException
import com.asuarezgalan.tvshowschallenge.common.domain.exceptions.MappingException
import com.asuarezgalan.tvshowschallenge.shows.data.mocks.ShowsDataMockEntitiesProvider
import com.asuarezgalan.tvshowschallenge.shows.data.network.ShowsNetworkDataSource
import com.asuarezgalan.tvshowschallenge.shows.domain.ShowsRepository
import io.mockk.every
import io.mockk.mockk
import io.reactivex.Single
import org.junit.Test

import org.junit.Before
import java.lang.NullPointerException

class ShowsRepositoryTest {

    private val showNetworkDsMock = mockk<ShowsNetworkDataSource>()
    private lateinit var repository: ShowsRepository

    @Before
    fun setUp() {
        repository = ShowsRepositoryImpl(showNetworkDsMock)
    }

    /**
     * Tests that repository will successfully map data entities
     * and will return the correct domain entities
     */
    @Test
    fun retrieveShowsFromNetwork_Success() {
        val mockPage = ShowsDataMockEntitiesProvider.getPaginatedResponse()
        every { showNetworkDsMock.getTvShows(any<Int>()) } returns Single.just(mockPage)

        val subscriber = repository.retrieveShowsFromNetwork(1).test()

        with(subscriber) {
            assertSubscribed()
            awaitTerminalEvent()
            assertValueCount(1)
            assertComplete()
        }

    }

    /**
     * Tests that repository will propagate any errors caused by mapping process
     * mapping them into known exceptions
     */
    @Test
    fun retrieveShowsFromNetwork_FailureNullPointer() {
        every { showNetworkDsMock.getTvShows(any<Int>()) } returns Single.error(NullPointerException())

        val subscriber = repository.retrieveShowsFromNetwork(1).test()

        with(subscriber) {
            assertSubscribed()
            awaitTerminalEvent()
            assertError(MappingException::class.java)
            assertTerminated()
        }

    }

    /**
     * Same as previous test, but tests that it will not transform incoming exceptions from Data source
     */
    @Test
    fun retrieveShowsFromNetwork_FailureOther() {
        every { showNetworkDsMock.getTvShows(any<Int>()) } returns Single.error(HostNotFoundException())

        val subscriber = repository.retrieveShowsFromNetwork(1).test()

        with(subscriber) {
            assertSubscribed()
            awaitTerminalEvent()
            assertError(HostNotFoundException::class.java)
            assertTerminated()
        }

    }

}