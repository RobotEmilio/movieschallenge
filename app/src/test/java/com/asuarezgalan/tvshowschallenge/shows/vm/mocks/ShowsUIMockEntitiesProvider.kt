package com.asuarezgalan.tvshowschallenge.shows.vm.mocks

import com.asuarezgalan.tvshowschallenge.shows.ui.model.ShowUi

/**
 * Returns UI objects to be used in tests (not mocks since they're plain data objects)
 */
object ShowsUIMockEntitiesProvider {

    fun getShowUi() : ShowUi =
        ShowUi(1, "title", "overview", 5f, "baseUrl"+"size"+"imageId")

}