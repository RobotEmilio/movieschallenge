package com.asuarezgalan.tvshowschallenge.shows.domain.mocks

import com.asuarezgalan.tvshowschallenge.shows.domain.model.Show
import com.asuarezgalan.tvshowschallenge.shows.domain.model.ShowListPage

/**
 * Returns domain objects to be used in tests (not mocks since they're plain data objects)
 */
object ShowsDomainMockEntitiesProvider {

    fun getShowListPage() : ShowListPage = ShowListPage(false, listOf(getShow()))

    fun getShow(): Show = Show(1, "title", "overview", 5f, "imageId")


}