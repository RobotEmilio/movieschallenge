package com.asuarezgalan.tvshowschallenge.shows.vm.mapper

import com.asuarezgalan.tvshowschallenge.shows.domain.mocks.ShowsDomainMockEntitiesProvider
import com.asuarezgalan.tvshowschallenge.shows.ui.model.ShowUi
import com.asuarezgalan.tvshowschallenge.shows.vm.mocks.ShowsUIMockEntitiesProvider
import org.junit.Test

class ShowsUiMapperTest {

    /**
     * Tests that mapper will return the expected UI entity
     */
    @Test
    fun mapTo_Success() {
        val expectedResult = ShowsUIMockEntitiesProvider.getShowUi()
        val mockValue = ShowsDomainMockEntitiesProvider.getShow()

        val result = ShowsUiMapper.mapTo(mockValue)

        assert((expectedResult is ShowUi) == (result is ShowUi))
    }

}