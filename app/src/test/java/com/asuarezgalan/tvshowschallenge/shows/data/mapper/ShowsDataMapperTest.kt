package com.asuarezgalan.tvshowschallenge.shows.data.mapper

import com.asuarezgalan.tvshowschallenge.shows.data.mocks.ShowsDataMockEntitiesProvider
import com.asuarezgalan.tvshowschallenge.shows.domain.mocks.ShowsDomainMockEntitiesProvider
import com.asuarezgalan.tvshowschallenge.shows.domain.model.ShowListPage
import org.junit.Test

class ShowsDataMapperTest {

    /**
     * Tests that mapper will return the expected domain entity
     *
     * In this case, it will not be the last page
     */
    @Test
    fun showsDataMapperMapTo_Success() {
        val pageResponse = ShowsDataMockEntitiesProvider.getPaginatedResponse()
        val expectedResult = ShowsDomainMockEntitiesProvider.getShowListPage()

        val result = ShowsDataMapper.mapTo(pageResponse)

        val expectedResultValidation : Boolean = expectedResult is ShowListPage
        val resultValidation : Boolean = result is ShowListPage
        assert(expectedResultValidation == resultValidation)
        assert(!result.isLastPage)
    }

    /**
     * Tests that mapper will return the expected domain
     *
     * In this case, it should return lastPage as true
     */
    @Test
    fun showsDataMapperMapTo_SuccessLastPage() {
        val pageResponse = ShowsDataMockEntitiesProvider.getPaginatedResponse().copy(page = 200)
        val expectedResult = ShowsDomainMockEntitiesProvider.getShowListPage().copy(isLastPage = true)

        val result = ShowsDataMapper.mapTo(pageResponse)

        assert(expectedResult.isLastPage == result.isLastPage)
    }
}