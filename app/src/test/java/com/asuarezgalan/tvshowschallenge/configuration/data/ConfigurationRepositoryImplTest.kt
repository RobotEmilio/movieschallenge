package com.asuarezgalan.tvshowschallenge.configuration.data

import com.asuarezgalan.tvshowschallenge.common.data.network.exceptions.HostNotFoundException
import com.asuarezgalan.tvshowschallenge.common.domain.exceptions.MappingException
import com.asuarezgalan.tvshowschallenge.configuration.data.mocks.ConfigurationDataMockEntitiesProvider
import com.asuarezgalan.tvshowschallenge.configuration.data.network.ConfigurationNetworkDataSource
import com.asuarezgalan.tvshowschallenge.configuration.domain.ConfigurationRepository
import com.asuarezgalan.tvshowschallenge.configuration.domain.model.Configuration
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Single
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import java.lang.NullPointerException

class ConfigurationRepositoryImplTest {

    private val mockNetworkDataSource : ConfigurationNetworkDataSource = mockk()
    private lateinit var testSubject : ConfigurationRepository

    @Before
    fun setUp() {
        testSubject = ConfigurationRepositoryImpl(mockNetworkDataSource)
    }

    /**
     * Tests that repository will successfully map data entities
     * and will return the correct domain entities
     */
    @Test
    fun getConfiguration_Success() {
        val mockConfig = ConfigurationDataMockEntitiesProvider.getConfigurationResponse()
        every { mockNetworkDataSource.getConfiguration() } returns Single.just(mockConfig)

        val testSubscriber = testSubject.getConfiguration().test()

        verify(exactly = 1) { mockNetworkDataSource.getConfiguration() }
        with(testSubscriber) {
            assertSubscribed()
            awaitTerminalEvent()
            assertValueCount(1)
            assertValue { it is Configuration }
            assertComplete()
        }
    }

    /**
     * Tests that repository will propagate any errors caused by mapping process
     * mapping them into known exceptions
     */
    @Test
    fun getConfiguration_MapFailure() {
        every { mockNetworkDataSource.getConfiguration() } returns Single.error(NullPointerException())

        val testSubscriber = testSubject.getConfiguration().test()

        verify(exactly = 1) { mockNetworkDataSource.getConfiguration() }
        with(testSubscriber) {
            assertSubscribed()
            awaitTerminalEvent()
            assertError(MappingException::class.java)
        }
    }

    /**
     * Same as previous test, but tests that it will not transform incoming exceptions from Data source
     */
    @Test
    fun getConfiguration_Failure() {
        every { mockNetworkDataSource.getConfiguration() } returns Single.error(HostNotFoundException())

        val testSubscriber = testSubject.getConfiguration().test()

        verify(exactly = 1) { mockNetworkDataSource.getConfiguration() }
        with(testSubscriber) {
            assertSubscribed()
            awaitTerminalEvent()
            assertError(HostNotFoundException::class.java)
        }
    }
}