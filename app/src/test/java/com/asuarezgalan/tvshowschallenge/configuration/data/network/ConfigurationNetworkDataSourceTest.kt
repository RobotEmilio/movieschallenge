package com.asuarezgalan.tvshowschallenge.configuration.data.network

import android.util.Base64
import com.asuarezgalan.tvshowschallenge.common.data.network.exceptions.HostNotFoundException
import com.asuarezgalan.tvshowschallenge.configuration.data.mocks.ConfigurationDataMockEntitiesProvider
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import io.mockk.verify
import io.reactivex.Single
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import java.net.UnknownHostException

class ConfigurationNetworkDataSourceTest {

    private val mockApi : ConfigurationApi = mockk()
    private lateinit var subjectTest : ConfigurationNetworkDataSource

    @Before
    fun setUp() {
        mockkStatic(Base64::class)
        every { Base64.decode(any<String>(), any()) } returns "Key".toByteArray()

        subjectTest = ConfigurationNetworkDataSource(mockApi)
    }

    /**
     * Tests that datasource will return successfuly configuration response entities from API
     */
    @Test
    fun getConfiguration_Success() {
        val response = ConfigurationDataMockEntitiesProvider.getImagesResponse()
        every { mockApi.getConfiguration(any()) } returns Single.just(response)

        val testSubscriber = subjectTest.getConfiguration().test()

        verify(exactly = 1) { mockApi.getConfiguration(any())}
        with(testSubscriber) {
            assertSubscribed()
            awaitTerminalEvent()
            assertValueCount(1)
            assertComplete()
        }
    }

    /**
     * Tests that datasource will propagate errors caused by API
     */
    @Test
    fun getConfiguration_Error() {
        every { mockApi.getConfiguration(any()) } returns Single.error(Throwable())

        val testSubscriber = subjectTest.getConfiguration().test()

        verify(exactly = 1) { mockApi.getConfiguration(any())}
        with(testSubscriber) {
            assertSubscribed()
            awaitTerminalEvent()
            assertError(Throwable::class.java)
        }
    }

    /**
     * Tests that datasource will send custom exception when no connection is found
     */
    @Test
    fun getConfiguration_ErrorNetwork() {
        every { mockApi.getConfiguration(any()) } returns Single.error(UnknownHostException())

        val testSubscriber = subjectTest.getConfiguration().test()

        verify(exactly = 1) { mockApi.getConfiguration(any())}
        with(testSubscriber) {
            assertSubscribed()
            awaitTerminalEvent()
            assertError(HostNotFoundException::class.java)
        }
    }
}