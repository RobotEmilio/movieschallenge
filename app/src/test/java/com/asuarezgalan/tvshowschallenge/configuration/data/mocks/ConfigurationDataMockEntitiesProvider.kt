package com.asuarezgalan.tvshowschallenge.configuration.data.mocks

import com.asuarezgalan.tvshowschallenge.configuration.data.network.model.ConfigurationResponse
import com.asuarezgalan.tvshowschallenge.configuration.data.network.model.ImagesResponse

/**
 * Returns data objects to be used in tests (not mocks since they're plain data objects)
 */
object ConfigurationDataMockEntitiesProvider {

    fun getConfigurationResponse() : ConfigurationResponse {
        return ConfigurationResponse("unsafeBaseUrl", "baseUrl", listOf("size"))
    }

    fun getImagesResponse() : ImagesResponse {
        return ImagesResponse(getConfigurationResponse())
    }

}