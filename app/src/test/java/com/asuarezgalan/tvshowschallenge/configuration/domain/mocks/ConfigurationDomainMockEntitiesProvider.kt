package com.asuarezgalan.tvshowschallenge.configuration.domain.mocks

import com.asuarezgalan.tvshowschallenge.configuration.domain.model.Configuration

/**
 * Returns domain objects to be used in tests (not mocks since they're plain data objects)
 */
object ConfigurationDomainMockEntitiesProvider {
    fun getConfiguration() : Configuration = Configuration("baseUrl", "size")
}