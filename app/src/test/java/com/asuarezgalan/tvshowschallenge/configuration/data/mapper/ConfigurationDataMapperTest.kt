package com.asuarezgalan.tvshowschallenge.configuration.data.mapper

import com.asuarezgalan.tvshowschallenge.configuration.data.mocks.ConfigurationDataMockEntitiesProvider
import com.asuarezgalan.tvshowschallenge.configuration.domain.mocks.ConfigurationDomainMockEntitiesProvider
import com.asuarezgalan.tvshowschallenge.configuration.domain.model.Configuration
import org.junit.Test

import org.junit.Assert.*

class ConfigurationDataMapperTest {

    /**
     * Tests that mapper will return the expected domain entity
     */
    @Test
    fun mapTo_Success() {
        val expectedResult = ConfigurationDomainMockEntitiesProvider.getConfiguration()
        val configResponseMock = ConfigurationDataMockEntitiesProvider.getConfigurationResponse()

        val result = ConfigurationDataMapper.mapTo(configResponseMock)

        assert((expectedResult is Configuration) == (result is Configuration))
    }
}