package com.asuarezgalan.tvshowschallenge.configuration.domain.model

data class Configuration(
    val baseUrl: String,
    val listCoverImageSize: String?
)