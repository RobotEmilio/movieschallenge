package com.asuarezgalan.tvshowschallenge.configuration.domain

import com.asuarezgalan.tvshowschallenge.configuration.domain.model.Configuration
import io.reactivex.Single

/**
 * This class is responsible for managing sources that deal with Configuration entity
 *
 * NOTE = Ideally, it would also have managed a caching source, like a DB or sharedPreferences
 */
interface ConfigurationRepository {

    /**
     * This method retrieves a stream containing the different configurations retrieved from server
     * and maps them into a Configuration entity containing only business-needed configs
     */
    fun getConfiguration(): Single<Configuration>
}