package com.asuarezgalan.tvshowschallenge.configuration.di

import com.asuarezgalan.tvshowschallenge.configuration.data.network.ConfigurationApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class ConfigurationDataSourceModule {

    @Provides
    fun provideConfigurationApi(retrofit: Retrofit) : ConfigurationApi = retrofit.create(ConfigurationApi::class.java)

}