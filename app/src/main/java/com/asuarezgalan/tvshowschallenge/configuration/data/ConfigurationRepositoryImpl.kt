package com.asuarezgalan.tvshowschallenge.configuration.data

import com.asuarezgalan.tvshowschallenge.common.domain.exceptions.MappingException
import com.asuarezgalan.tvshowschallenge.rx.mapErrors
import com.asuarezgalan.tvshowschallenge.rx.subscribeOnIo
import com.asuarezgalan.tvshowschallenge.configuration.data.mapper.ConfigurationDataMapper
import com.asuarezgalan.tvshowschallenge.configuration.data.network.ConfigurationNetworkDataSource
import com.asuarezgalan.tvshowschallenge.configuration.domain.ConfigurationRepository
import com.asuarezgalan.tvshowschallenge.configuration.domain.model.Configuration
import io.reactivex.Single
import java.lang.NullPointerException
import javax.inject.Inject

class ConfigurationRepositoryImpl @Inject constructor(private val networkDs: ConfigurationNetworkDataSource) : ConfigurationRepository {

    override fun getConfiguration(): Single<Configuration> =
        networkDs.getConfiguration().subscribeOnIo()
            .map { ConfigurationDataMapper.mapTo(it) }
            .mapErrors(::transformErrors)

    /**
     * Maps risen exceptions to business-logic exceptions
     */
    private fun transformErrors(error: Throwable): Throwable {
        return when (error) {
            is NullPointerException -> MappingException()
            else -> error
        }
    }

}