package com.asuarezgalan.tvshowschallenge.configuration.data.network.model

import com.google.gson.annotations.SerializedName

data class ImagesResponse(@SerializedName("images") val config: ConfigurationResponse)