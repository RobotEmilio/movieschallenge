package com.asuarezgalan.tvshowschallenge.configuration.data.network

import com.asuarezgalan.tvshowschallenge.configuration.data.network.model.ImagesResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ConfigurationApi {

    @GET("configuration")
    fun getConfiguration(@Query("api_key") apiKey: String) : Single<ImagesResponse>
}