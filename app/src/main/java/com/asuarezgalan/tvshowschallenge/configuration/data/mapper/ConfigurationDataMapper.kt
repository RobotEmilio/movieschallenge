package com.asuarezgalan.tvshowschallenge.configuration.data.mapper

import com.asuarezgalan.tvshowschallenge.configuration.data.network.model.ConfigurationResponse
import com.asuarezgalan.tvshowschallenge.configuration.domain.model.Configuration

object ConfigurationDataMapper {

    /**
     * We only want one size for the Show View cover image. This number indicates the position in the
     * poster size array incoming from server. The higher the number, the higher the resolution
     */
    private const val SHOWS_LIST_COVER_SIZE_INDEX = 2

    /**
     * Given a configuration data entity, returns the corresponding domain model entity
     */
    fun mapTo(item: ConfigurationResponse): Configuration = Configuration(
        baseUrl = item.secureBaseUrl,
        listCoverImageSize = item.posterSizes.getOrNull(SHOWS_LIST_COVER_SIZE_INDEX)
    )
}