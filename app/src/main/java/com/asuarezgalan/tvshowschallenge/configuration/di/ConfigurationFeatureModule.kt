package com.asuarezgalan.tvshowschallenge.configuration.di

import dagger.Module

@Module(includes = [ConfigurationDataSourceModule::class, ConfigurationRepositoryModule::class])
abstract class ConfigurationFeatureModule