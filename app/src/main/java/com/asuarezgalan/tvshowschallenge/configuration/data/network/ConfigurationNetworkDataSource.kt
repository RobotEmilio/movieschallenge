package com.asuarezgalan.tvshowschallenge.configuration.data.network

import com.asuarezgalan.tvshowschallenge.BuildConfig
import com.asuarezgalan.tvshowschallenge.common.data.network.exceptions.HostNotFoundException
import com.asuarezgalan.tvshowschallenge.common.domain.base64decode
import com.asuarezgalan.tvshowschallenge.rx.mapErrors
import com.asuarezgalan.tvshowschallenge.configuration.data.network.model.ConfigurationResponse
import io.reactivex.Single
import java.net.UnknownHostException
import javax.inject.Inject

/**
 * This class is responsible for connecting to configuration API
 */
class ConfigurationNetworkDataSource @Inject constructor(private val api: ConfigurationApi) {

    /**
     * Returns a stream containing the API configuration
     */
    fun getConfiguration(): Single<ConfigurationResponse> =
        api.getConfiguration(BuildConfig.API_KEY.base64decode()).map { it.config }.mapErrors(::transformError)

    /**
     * Maps risen exceptions to business-logic exceptions
     */
    private fun transformError(throwable: Throwable): Throwable {
        return when (throwable) {
            is UnknownHostException -> HostNotFoundException(throwable)
            else -> throwable
        }
    }

}