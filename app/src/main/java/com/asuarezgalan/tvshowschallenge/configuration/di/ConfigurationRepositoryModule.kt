package com.asuarezgalan.tvshowschallenge.configuration.di

import com.asuarezgalan.tvshowschallenge.configuration.data.ConfigurationRepositoryImpl
import com.asuarezgalan.tvshowschallenge.configuration.domain.ConfigurationRepository
import dagger.Binds
import dagger.Module

@Module
abstract class ConfigurationRepositoryModule {

    @Binds
    abstract fun bindConfigurationRepository(implementation: ConfigurationRepositoryImpl) : ConfigurationRepository

}