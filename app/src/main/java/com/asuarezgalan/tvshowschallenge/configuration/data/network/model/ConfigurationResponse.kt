package com.asuarezgalan.tvshowschallenge.configuration.data.network.model

import com.google.gson.annotations.SerializedName

data class ConfigurationResponse(
    @SerializedName("base_url") val baseUrl: String,
    @SerializedName("secure_base_url") val secureBaseUrl: String,
    @SerializedName("poster_sizes") val posterSizes: List<String>
)