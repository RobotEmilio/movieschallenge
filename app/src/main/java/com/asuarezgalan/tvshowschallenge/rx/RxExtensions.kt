package com.asuarezgalan.tvshowschallenge.rx

import com.asuarezgalan.tvshowschallenge.common.vm.UiAction
import com.hadilq.liveevent.LiveEvent
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Extension function for sending loading events for a single stream in a viewmodel
 */
fun <T> Single<T>.applyLoadings(liveData: LiveEvent<UiAction>?): Single<T> =
    doOnSubscribe { liveData?.value = UiAction.Loading(true) }
        .doOnEvent { _, _ -> liveData?.value = UiAction.Loading(false) }

/**
 * Utility extension function for shorten subscribing to IO scheduler
 */
fun <T> Single<T>.subscribeOnIo(): Single<T> = subscribeOn(Schedulers.io())

/**
 * Utility extension function for shorten observing on main thread
 */
fun <T> Single<T>.observeOnMainThread(): Single<T> = observeOn(AndroidSchedulers.mainThread())

/**
 * Extension function for shorten mapping exceptions
 */
fun <T> Single<T>.mapErrors(errorTransformer: (Throwable) -> Throwable): Single<T> =
    onErrorResumeNext { error ->
        Single.error(errorTransformer(error))
    }