package com.asuarezgalan.tvshowschallenge.di

import android.content.Context
import com.asuarezgalan.tvshowschallenge.ShowsApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ShowsMainModule {

    @Singleton
    @Provides
    fun provideApplicationContext(application: ShowsApplication): Context {
        return application.applicationContext
    }

}