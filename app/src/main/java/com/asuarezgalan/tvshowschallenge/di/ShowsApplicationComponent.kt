package com.asuarezgalan.tvshowschallenge.di

import com.asuarezgalan.tvshowschallenge.ShowsApplication
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ShowsMainModule::class,
        NetworkModule::class,
        FeaturesModule::class
    ]
)
interface ShowsApplicationComponent : AndroidInjector<ShowsApplication> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<ShowsApplication>()


}