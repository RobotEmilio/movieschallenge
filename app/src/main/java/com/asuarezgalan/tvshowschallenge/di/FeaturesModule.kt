package com.asuarezgalan.tvshowschallenge.di

import androidx.lifecycle.ViewModelProvider
import com.asuarezgalan.tvshowschallenge.configuration.di.ConfigurationDataSourceModule
import com.asuarezgalan.tvshowschallenge.configuration.di.ConfigurationFeatureModule
import com.asuarezgalan.tvshowschallenge.di.vm.ViewModelFactoryProvider
import com.asuarezgalan.tvshowschallenge.shows.di.ShowsFeatureModule
import dagger.Binds
import dagger.Module

@Module(includes = [ShowsFeatureModule::class, ConfigurationFeatureModule::class])
abstract class FeaturesModule {

    @Binds
    abstract fun bindViewModelFactory(provider: ViewModelFactoryProvider): ViewModelProvider.Factory

}