package com.asuarezgalan.tvshowschallenge.shows.vm

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.asuarezgalan.tvshowschallenge.rx.applyLoadings
import com.asuarezgalan.tvshowschallenge.rx.observeOnMainThread
import com.asuarezgalan.tvshowschallenge.common.vm.BaseViewModel
import com.asuarezgalan.tvshowschallenge.common.vm.UiAction
import com.asuarezgalan.tvshowschallenge.shows.domain.usecase.GetTvShowsUseCase
import com.asuarezgalan.tvshowschallenge.shows.ui.ShowListFragment.ShowListFragmentActions.*
import com.asuarezgalan.tvshowschallenge.shows.ui.model.ShowUi
import com.asuarezgalan.tvshowschallenge.shows.vm.mapper.ShowsUiMapper
import com.hadilq.liveevent.LiveEvent
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

class ShowsViewModel @Inject constructor(
    private val getTvShowsUseCase: GetTvShowsUseCase
) : BaseViewModel() {

    /**
     * Live data that receives shows to be shown by the UI
     */
    private val _showsLiveData = MutableLiveData<List<ShowUi>>()
    val showsLiveData: LiveData<List<ShowUi>>
        get() = _showsLiveData

    /**
     * Live data that receives Single Live events
     * This way it can notify UI, but it won't resend the data every time the ui reobserves
     */
    private val _event: LiveEvent<UiAction> = LiveEvent()
    val events: LiveData<UiAction>
        get() = _event

    /**
     * Executes GetTvShowsUseCase to retrieve another page of shows
     *
     * Once the data arrives, it will be served to UI via livedatas
     */
    fun getTvShows(page: Int) {
        getTvShowsUseCase.execute(page)
            .observeOnMainThread()
            .applyLoadings(_event)
            .subscribeBy(this::handleErrors) {
                if (it.isLastPage) {
                    _event.value = LastPage
                }
                _showsLiveData.value = it.results.map(ShowsUiMapper::mapTo)
            }
            .also(::addDisposable)
    }

    private fun handleErrors(error: Throwable) {
        _event.value = UiAction.Error(error)
    }

}