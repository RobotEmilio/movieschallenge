package com.asuarezgalan.tvshowschallenge.shows.ui.adapter

import android.view.ViewGroup
import com.asuarezgalan.tvshowschallenge.common.ui.recyclerview.BaseRecyclerAdapter
import com.asuarezgalan.tvshowschallenge.common.ui.recyclerview.BaseViewHolder
import com.asuarezgalan.tvshowschallenge.shows.ui.model.ShowUi
import com.asuarezgalan.tvshowschallenge.shows.ui.widget.ShowView

class ShowsAdapter :
    BaseRecyclerAdapter<ShowUi, ShowsAdapter.ShowsViewHolder>({ old, new -> old.id == new.id }) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShowsViewHolder {
        return ShowsViewHolder(ShowView(parent.context))
    }

    class ShowsViewHolder(private val view: ShowView) : BaseViewHolder<ShowUi>(view) {

        override fun bind(item: ShowUi) {
            with(view) {
                setTitle(item.title)
                setOverview(item.overview)
                setRating(item.score)
                setCoverImage(item.coverUrl)
            }
        }

    }

}