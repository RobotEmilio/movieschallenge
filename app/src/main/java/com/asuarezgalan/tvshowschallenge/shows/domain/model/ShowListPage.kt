package com.asuarezgalan.tvshowschallenge.shows.domain.model

data class ShowListPage(
    val isLastPage: Boolean,
    val results: List<Show>
)