package com.asuarezgalan.tvshowschallenge.shows.data.network

import com.asuarezgalan.tvshowschallenge.BuildConfig
import com.asuarezgalan.tvshowschallenge.common.data.network.exceptions.HostNotFoundException
import com.asuarezgalan.tvshowschallenge.common.domain.base64decode
import com.asuarezgalan.tvshowschallenge.common.data.network.model.PaginatedResponse
import com.asuarezgalan.tvshowschallenge.rx.mapErrors
import com.asuarezgalan.tvshowschallenge.shows.data.network.model.ShowListResponse
import io.reactivex.Single
import java.net.UnknownHostException
import javax.inject.Inject

/**
 * This class is responsible for connecting to shows API
 */
class ShowsNetworkDataSource @Inject constructor(
    private val api: ShowsTvApi
) {

    /**
     * Return a stream containing shows
     */
    fun getTvShows(page: Int): Single<PaginatedResponse<ShowListResponse>> {
        return api.getTvShowsList(BuildConfig.API_KEY.base64decode(), page).mapErrors(::transformError)
    }

    /**
     * Maps risen exceptions to business-logic exceptions
     */
    private fun transformError(throwable: Throwable): Throwable {
        return when (throwable) {
            is UnknownHostException -> HostNotFoundException(throwable)
            else -> throwable
        }
    }

}