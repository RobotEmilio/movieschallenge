package com.asuarezgalan.tvshowschallenge.shows.data.mapper

import com.asuarezgalan.tvshowschallenge.common.data.network.model.PaginatedResponse
import com.asuarezgalan.tvshowschallenge.shows.data.network.model.ShowListResponse
import com.asuarezgalan.tvshowschallenge.shows.domain.model.Show
import com.asuarezgalan.tvshowschallenge.shows.domain.model.ShowListPage

/**
 * Maps shows datasource entities into domain entities
 */
object ShowsDataMapper {

    fun mapTo(item: ShowListResponse): Show = Show(
        id = item.id,
        title = item.title,
        overView = item.overview,
        coverUrl = item.coverUrl,
        score = item.score
    )

    fun mapTo(item: PaginatedResponse<ShowListResponse>): ShowListPage = ShowListPage(
        item.page >= item.totalPages,
        item.result.map(::mapTo)
    )
}