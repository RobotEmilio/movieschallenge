package com.asuarezgalan.tvshowschallenge.shows.di

import com.asuarezgalan.tvshowschallenge.shows.data.ShowsRepositoryImpl
import com.asuarezgalan.tvshowschallenge.shows.domain.ShowsRepository
import dagger.Binds
import dagger.Module

@Module
abstract class ShowsRepositoryModule {

    @Binds
    abstract fun bindsShowsRepository(implementation: ShowsRepositoryImpl) : ShowsRepository

}