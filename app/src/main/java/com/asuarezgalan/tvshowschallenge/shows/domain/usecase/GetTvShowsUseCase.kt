package com.asuarezgalan.tvshowschallenge.shows.domain.usecase

import com.asuarezgalan.tvshowschallenge.configuration.domain.ConfigurationRepository
import com.asuarezgalan.tvshowschallenge.configuration.domain.model.Configuration
import com.asuarezgalan.tvshowschallenge.shows.domain.exceptions.MissingConfigurationException
import com.asuarezgalan.tvshowschallenge.shows.domain.ShowsRepository
import com.asuarezgalan.tvshowschallenge.shows.domain.model.ShowListPage
import io.reactivex.Single
import io.reactivex.SingleSource
import io.reactivex.exceptions.Exceptions
import io.reactivex.functions.BiFunction
import javax.inject.Inject

/**
 * This class is responsible for supplying complete and correct show entities
 */
class GetTvShowsUseCase @Inject constructor(
    private val showsRepo: ShowsRepository,
    private val configurationRepo: ConfigurationRepository
) {

    /**
     * Executes the use case: Retrieves both show entities and its configuration
     * and completes the cover url
     */
    fun execute(page: Int): Single<ShowListPage> =
        Single.zip(getShows(page), getConfiguration(), zipFunction)

    /**
     * Retrieves show entities from its repository
     */
    private fun getShows(page: Int): SingleSource<out ShowListPage> =
        showsRepo.retrieveShowsFromNetwork(page)

    /**
     * Retrieves configuration from its repository.
     *
     * If it can't do this, it will return an empty configuration object
     * This is done in order that only shows can fail, so combining the two streams
     * won't generate two exceptions
     */
    private fun getConfiguration(): Single<Configuration> =
        configurationRepo.getConfiguration()
            .onErrorReturn { Configuration("", "") }

    /**
     * Combines shows entities with configuration
     *
     * What it does internally is:
     *  - crash in case configuration couldn't be retrieved
     *  - Concatenate final image url like follows: baseUrl + imageSizeModifier + image Id
     */
    private val zipFunction =
        BiFunction<ShowListPage, Configuration, ShowListPage> { shows, config ->
            if (config.baseUrl.isBlank()) {
                throw MissingConfigurationException()
            }
            val modifiedResults = shows.results.map {
                    it.copy(coverUrl = config.baseUrl + config.listCoverImageSize + it.coverUrl)
                }
            shows.copy(results = modifiedResults)
        }
}