package com.asuarezgalan.tvshowschallenge.shows.di

import com.asuarezgalan.tvshowschallenge.shows.data.network.ShowsTvApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class ShowsDataSourceModule {

    @Provides
    fun provideShowsTvApi(retrofit: Retrofit): ShowsTvApi = retrofit.create(ShowsTvApi::class.java)

}