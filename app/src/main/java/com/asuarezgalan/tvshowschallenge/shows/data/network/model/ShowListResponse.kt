package com.asuarezgalan.tvshowschallenge.shows.data.network.model

import com.google.gson.annotations.SerializedName

data class ShowListResponse(
    @SerializedName("id") val id: Int,
    @SerializedName("name") val title: String,
    @SerializedName("overview") val overview: String,
    @SerializedName("poster_path") val coverUrl: String?,
    @SerializedName("vote_average") val score: Float
)