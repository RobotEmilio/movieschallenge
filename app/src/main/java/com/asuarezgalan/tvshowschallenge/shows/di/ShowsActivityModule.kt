package com.asuarezgalan.tvshowschallenge.shows.di

import com.asuarezgalan.tvshowschallenge.shows.ui.ShowListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ShowsActivityModule {

    @ContributesAndroidInjector
    abstract fun contributeMovieListFragment(): ShowListFragment

}