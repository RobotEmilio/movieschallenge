package com.asuarezgalan.tvshowschallenge.shows.domain.model

data class Show(
    val id: Int,
    val title: String,
    val overView: String,
    val score: Float,
    val coverUrl: String?
)