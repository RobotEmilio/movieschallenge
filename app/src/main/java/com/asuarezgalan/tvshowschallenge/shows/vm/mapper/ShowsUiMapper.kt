package com.asuarezgalan.tvshowschallenge.shows.vm.mapper

import com.asuarezgalan.tvshowschallenge.shows.domain.model.Show
import com.asuarezgalan.tvshowschallenge.shows.ui.model.ShowUi

/**
 * Maps shows domain entities into UI entities
 */
object ShowsUiMapper {

    fun mapTo(item: Show) : ShowUi = ShowUi(
        id = item.id,
        title = item.title,
        coverUrl = item.coverUrl,
        score = item.score,
        overview = item.overView
    )
}