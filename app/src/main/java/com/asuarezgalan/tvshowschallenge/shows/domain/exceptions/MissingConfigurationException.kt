package com.asuarezgalan.tvshowschallenge.shows.domain.exceptions

import java.lang.Exception

class MissingConfigurationException(
    override val cause: Throwable? = null
) : Exception()