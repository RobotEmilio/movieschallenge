package com.asuarezgalan.tvshowschallenge.shows.ui.widget

import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import com.asuarezgalan.tvshowschallenge.R
import com.asuarezgalan.tvshowschallenge.common.ui.inflate
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.widget_movie_list_item.view.*

class ShowView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null
) : ConstraintLayout(context, attrs) {

    init {
        inflate(R.layout.widget_movie_list_item, true).apply {
            layoutParams = LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        }
    }

    fun setTitle(title: String) {
        movie_title?.text = title
    }

    fun setCoverImage(coverUrl: String?) {
        coverUrl?.let {
            Glide.with(this)
                .load(coverUrl)
                .placeholder(R.drawable.ic_movie_placeholder)
                .centerCrop()
                .into(movie_cover)
        }
    }

    fun setOverview(overview: String) {
        movie_overview?.text = overview
    }

    fun setRating(score: Float) {
        movie_score?.text = resources.getString(R.string.movie_score, score)
    }


}