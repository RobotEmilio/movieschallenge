package com.asuarezgalan.tvshowschallenge.shows.data

import com.asuarezgalan.tvshowschallenge.common.domain.exceptions.MappingException
import com.asuarezgalan.tvshowschallenge.rx.mapErrors
import com.asuarezgalan.tvshowschallenge.rx.subscribeOnIo
import com.asuarezgalan.tvshowschallenge.shows.data.mapper.ShowsDataMapper
import com.asuarezgalan.tvshowschallenge.shows.data.network.ShowsNetworkDataSource
import com.asuarezgalan.tvshowschallenge.shows.domain.ShowsRepository
import com.asuarezgalan.tvshowschallenge.shows.domain.model.ShowListPage
import io.reactivex.Single
import java.lang.NullPointerException
import javax.inject.Inject

class ShowsRepositoryImpl @Inject constructor(private val networkDs: ShowsNetworkDataSource) :
    ShowsRepository {

    override fun retrieveShowsFromNetwork(page: Int): Single<ShowListPage> {
        return networkDs.getTvShows(page)
            .subscribeOnIo()
            .map(ShowsDataMapper::mapTo)
            .mapErrors(::transformErrors)
    }

    /**
     * Maps risen exceptions to business-logic exceptions
     */
    private fun transformErrors(error: Throwable): Throwable {
        return when (error) {
            is NullPointerException -> MappingException()
            else -> error
        }
    }

}