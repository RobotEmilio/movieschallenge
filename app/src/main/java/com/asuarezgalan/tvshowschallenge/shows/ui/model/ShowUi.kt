package com.asuarezgalan.tvshowschallenge.shows.ui.model

data class ShowUi(
    val id: Int,
    val title: String,
    val coverUrl: String?,
    val score: Float,
    val overview: String
)