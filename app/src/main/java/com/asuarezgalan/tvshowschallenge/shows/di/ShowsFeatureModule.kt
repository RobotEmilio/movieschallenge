package com.asuarezgalan.tvshowschallenge.shows.di

import androidx.lifecycle.ViewModel
import com.asuarezgalan.tvshowschallenge.di.ActivityScope
import com.asuarezgalan.tvshowschallenge.di.vm.ViewModelKey
import com.asuarezgalan.tvshowschallenge.shows.ui.ShowsActivity
import com.asuarezgalan.tvshowschallenge.shows.vm.ShowsViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module(includes = [ShowsDataSourceModule::class, ShowsRepositoryModule::class])
abstract class ShowsFeatureModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = [ShowsActivityModule::class])
    abstract fun contributeMainActivity() : ShowsActivity

    @Binds
    @IntoMap
    @ViewModelKey(ShowsViewModel::class)
    abstract fun bindMainViewModel(mainViewModel: ShowsViewModel) : ViewModel

}