package com.asuarezgalan.tvshowschallenge.shows.domain

import com.asuarezgalan.tvshowschallenge.shows.domain.model.ShowListPage
import io.reactivex.Single

/**
 * This class is responsible for managing sources that deal with Shows entities
 */
interface ShowsRepository {

    /**
     * This method retrieves a stream containing shows, fetching them in a background thread,
     * and transforming into domain model entities
     */
    fun retrieveShowsFromNetwork(page: Int): Single<ShowListPage>
}