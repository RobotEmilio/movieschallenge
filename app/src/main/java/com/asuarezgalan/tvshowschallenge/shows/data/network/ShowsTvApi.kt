package com.asuarezgalan.tvshowschallenge.shows.data.network

import com.asuarezgalan.tvshowschallenge.common.data.network.model.PaginatedResponse
import com.asuarezgalan.tvshowschallenge.shows.data.network.model.ShowListResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ShowsTvApi {

    @GET("tv/popular")
    fun getTvShowsList(@Query("api_key") apiKey: String, @Query("page") page: Int) : Single<PaginatedResponse<ShowListResponse>>
}