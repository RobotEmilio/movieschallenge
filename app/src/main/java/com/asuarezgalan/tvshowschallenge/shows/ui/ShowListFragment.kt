package com.asuarezgalan.tvshowschallenge.shows.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.asuarezgalan.tvshowschallenge.R
import com.asuarezgalan.tvshowschallenge.common.data.network.exceptions.HostNotFoundException
import com.asuarezgalan.tvshowschallenge.common.ui.*
import com.asuarezgalan.tvshowschallenge.common.ui.recyclerview.PaginationManager
import com.asuarezgalan.tvshowschallenge.common.vm.UiAction
import com.asuarezgalan.tvshowschallenge.shows.ui.adapter.ShowsAdapter
import com.asuarezgalan.tvshowschallenge.shows.ui.model.ShowUi
import com.asuarezgalan.tvshowschallenge.shows.vm.ShowsViewModel
import kotlinx.android.synthetic.main.fragment_movie_list.*
import javax.inject.Inject

class ShowListFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: ShowsViewModel

    private val showsAdapter = ShowsAdapter()
    private lateinit var paginationManager: PaginationManager

    companion object {
        const val PAGINATION_START = 1
        const val PAGINATION_THRESHOLD = 10
    }

    open class ShowListFragmentActions : UiAction() {
        //This event is sent whenever we've reached the last page of shows
        object LastPage : UiAction()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_movie_list, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = getViewModel(viewModelFactory)

        observeViewModel()

        setViews()

        paginationManager.start()
    }

    private fun observeViewModel() {
        viewModel.showsLiveData.observe(this, ::handleShows)
        viewModel.events.observe(this, ::handleEvents)
    }

    /**
     * Handle update on shows entities
     */
    private fun handleShows(shows: List<ShowUi>?) {
        shows?.let {
            showsAdapter.addData(shows)
            paginationManager.loadingPageSucceeded()
        }
    }

    /**
     * Handle Ui Events
     */
    private fun handleEvents(event: UiAction?) {
        event?.let {
            when (it) {
                is UiAction.Loading -> handleLoading(it)
                is UiAction.Error -> handleError(it)
                is ShowListFragmentActions.LastPage -> {
                    paginationManager.loadedLastPage()
                    showSnackBar(getString(R.string.last_page))
                }
            }
        }
    }

    /**
     * Toggles loader visibility
     */
    private fun handleLoading(loading: UiAction.Loading) {
        movies_swipe_refresh.isRefreshing = loading.visible
        paginationManager.setLoadingPage(loading.visible)
    }

    /**
     * Shows error messages to user
     */
    private fun handleError(error: UiAction.Error) {
        when (error.error) {
            is HostNotFoundException -> {
                if (paginationManager.lastPageLoaded()) {
                    getString(R.string.error_host_not_found).let(::showSnackBar)
                        .also { paginationManager.loadingPageFailed() }
                }
            }
            else -> {
                getString(R.string.error_unexpected_error).let(::showSnackBar)
                requireContext().logError(error.error)
            }
        }
    }

    private fun showSnackBar(message: String) {
        movies_swipe_refresh.showMessage(message)
    }

    /**
     * Set the ui element configuration
     */
    private fun setViews() {
        setSwipeLayout()
        setRecyclerView()
    }

    private fun setSwipeLayout() {
        with(movies_swipe_refresh) {
            setOnRefreshListener {
                showsAdapter.clearData()
                paginationManager.start()
            }
        }
    }

    private fun setRecyclerView() {
        with(movies_list) {
            val manager = LinearLayoutManager(context)
            paginationManager =
                PaginationManager(
                    linearLayoutManager = manager,
                    startingPage = PAGINATION_START,
                    threshold = PAGINATION_THRESHOLD
                ) {
                    viewModel.getTvShows(it)
                }.also(this::addOnScrollListener)
            layoutManager = manager
            adapter = showsAdapter
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        }
    }

}