package com.asuarezgalan.tvshowschallenge.common.ui

import dagger.android.support.DaggerAppCompatActivity

abstract class BaseActivity : DaggerAppCompatActivity()