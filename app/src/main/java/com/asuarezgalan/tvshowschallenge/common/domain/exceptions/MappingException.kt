package com.asuarezgalan.tvshowschallenge.common.domain.exceptions

import java.lang.Exception

/**
 * Exception risen whenever a mapper encounters a problem
 */
data class MappingException(
    override val cause: Throwable? = null
) : Exception()