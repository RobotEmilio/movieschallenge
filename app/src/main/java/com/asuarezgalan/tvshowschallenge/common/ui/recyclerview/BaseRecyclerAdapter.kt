package com.asuarezgalan.tvshowschallenge.common.ui.recyclerview

import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import kotlin.properties.Delegates

abstract class BaseRecyclerAdapter<T, U : BaseViewHolder<T>>(private val compare: (T, T) -> Boolean) :
    RecyclerView.Adapter<U>() {

    protected var data : MutableList<T> = mutableListOf()
        private set

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: U, position: Int) {
        data[position]?.let(holder::bind)
    }

    fun add(item: T) {
        val lastPosition = data.lastIndex.coerceAtLeast(0)
        data.add(item)
        notifyItemInserted(lastPosition)
    }

    fun addData(newData: List<T>) {
        val lastPosition = data.lastIndex.coerceAtLeast(0)
        data.addAll(newData)
        notifyItemRangeInserted(lastPosition+1, newData.size)
    }

    fun setData(newData: List<T>) = updateAndNotify(data, newData)

    fun replaceAt(item: T, position: Int) {
        data.set(position, item)
        notifyItemChanged(position)
    }

    fun removeAt(position: Int) {
        data.removeAt(position)
        notifyItemRemoved(position)
    }

    fun clearData() {
        updateAndNotify(data, mutableListOf())
    }

    /**
     * Calculates the diff between old and new data and
     * notifies adapter accordingly
     */
    private fun updateAndNotify(old: List<T>, new: List<T>) {
        val diff = DiffUtil.calculateDiff(object : DiffUtil.Callback() {

            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
                compare(old[oldItemPosition], new[newItemPosition])

            override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
                old[oldItemPosition] == new[newItemPosition]

            override fun getOldListSize() = old.size

            override fun getNewListSize() = new.size
        })
        data.clear()
        data = new.toMutableList()
        diff.dispatchUpdatesTo(this)

    }

}