package com.asuarezgalan.tvshowschallenge.common.data.network.model

import com.google.gson.annotations.SerializedName

/**
 * Response wrapper for TheMovieDb paginated response
 */
data class PaginatedResponse<T>(
    @SerializedName("page") val page: Int,
    @SerializedName("total_results") val totalResults: Int,
    @SerializedName("total_pages") val totalPages: Int,
    @SerializedName("results") val result: List<T>
)