package com.asuarezgalan.tvshowschallenge.common.domain

import android.util.Base64

/**
 * For a given encoded string, give its decoded representation
 *
 * Proguard should obscure this, so sensible information is not made public
 */
fun String.base64decode() : String {
    return Base64.decode(this, Base64.DEFAULT).toString(Charsets.UTF_8)
}