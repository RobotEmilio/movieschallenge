package com.asuarezgalan.tvshowschallenge.common.ui

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.*
import com.asuarezgalan.tvshowschallenge.BuildConfig
import com.google.android.material.snackbar.Snackbar
import dagger.android.support.DaggerAppCompatActivity
import dagger.android.support.DaggerFragment

/**
 * Extension method for encapsulate the retrieval of viewmodel inside an activity
 */
inline fun <reified T : ViewModel> DaggerAppCompatActivity.getViewModel(viewModelFactory: ViewModelProvider.Factory): T =
    ViewModelProviders.of(this, viewModelFactory).get(T::class.java)

/**
 * Extension method for encapsulate the retrieval of viewmodel inside an activity
 */
inline fun <reified T : ViewModel> DaggerFragment.getViewModel(viewModelFactory: ViewModelProvider.Factory): T =
    ViewModelProviders.of(this, viewModelFactory).get(T::class.java)

/**
 * Extension method for inflating a view
 */
fun ViewGroup.inflate(resourceId: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(resourceId, this, attachToRoot)
}

/**
 * Extension method for oberving a live data in viewmodel
 */
fun <T> LiveData<T>.observe(lifecycle: LifecycleOwner, body: (T?) -> Unit) {
    observe(lifecycle, Observer(body))
}

/**
 * Extension method for showing a message to the user
 */
fun View.showMessage(message: String) {
    Snackbar.make(this, message, Snackbar.LENGTH_SHORT).show()
}

/**
 * Extension method for showing errors in logcat
 */
fun Context.logError(error: Throwable) {
    if (BuildConfig.DEBUG) {
        Log.e(applicationContext.packageName, "Showing unhandled error", error)
    }
}