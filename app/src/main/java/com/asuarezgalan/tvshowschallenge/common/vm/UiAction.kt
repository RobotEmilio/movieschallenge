package com.asuarezgalan.tvshowschallenge.common.vm

open class UiAction {
    class Loading(val visible: Boolean) : UiAction()
    class Error(val error: Throwable) : UiAction()
}