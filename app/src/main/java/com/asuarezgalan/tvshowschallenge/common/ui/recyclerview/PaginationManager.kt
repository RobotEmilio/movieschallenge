package com.asuarezgalan.tvshowschallenge.common.ui.recyclerview

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

/**
 * Helper class that allows UIs with recyclerViews manage easily their pagination
 *
 * @param linearLayoutManager Recyclerview's manager
 * @param startingPage Numeric representation of the first page (probably 0 or 1, but depending on implementation)
 * @param threshold Number of remaining elements from which the helper will try to load new elements
 * @param loadMoreItems Lambda method that will be executed whenever new items are needed
 */
class PaginationManager(
    private val linearLayoutManager: LinearLayoutManager,
    private val startingPage: Int = 0,
    private var threshold: Int = 2,
    private val loadMoreItems: (Int) -> Unit
) : RecyclerView.OnScrollListener() {

    private var nextPage: Int = startingPage
    private var lastPage: Boolean = false
    private var isLoading: Boolean = false
    private var successPageLoad: Boolean = true

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        if (!isLoading && !lastPage) {
            val visibleItemCount = linearLayoutManager.childCount
            val totalItemCount = linearLayoutManager.itemCount
            val firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition()

            if ((visibleItemCount + firstVisibleItemPosition + threshold >= totalItemCount) && firstVisibleItemPosition >= 0) {
                loadMoreItems(nextPage)
            }
        }
    }

    /**
     * Restarts the pagination at 0
     */
    fun start() {
        nextPage = startingPage
        lastPage = false
        loadMoreItems(nextPage)
    }

    /**
     * Notify that requested page has successfully loaded
     */
    fun loadingPageSucceeded() {
        nextPage++
        successPageLoad = true
    }

    /**
     * Notify that requested page couldn't load
     */
    fun loadingPageFailed() {
        successPageLoad = false
    }

    /**
     * Notify that last page has been reached
     */
    fun loadedLastPage() {
        lastPage = true
    }

    /**
     * Returns if last page could be loaded
     */
    fun lastPageLoaded() : Boolean = successPageLoad

    /**
     * Tells paginator if a page is loading
     */
    fun setLoadingPage(loading: Boolean) {
        isLoading = loading
    }

}