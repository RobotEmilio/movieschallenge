package com.asuarezgalan.tvshowschallenge.common.data.network.exceptions

import java.lang.Exception

/**
 * Exception risen whenever the application cannot connect to remote host
 */
data class HostNotFoundException(
    override val cause: Throwable? = null
) : Exception()