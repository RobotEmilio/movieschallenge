package com.asuarezgalan.tvshowschallenge.common.ui

import dagger.android.support.DaggerFragment

abstract class BaseFragment : DaggerFragment()