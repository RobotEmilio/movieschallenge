package com.asuarezgalan.tvshowschallenge


import android.util.Log
import com.asuarezgalan.tvshowschallenge.di.DaggerShowsApplicationComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import io.reactivex.exceptions.UndeliverableException
import io.reactivex.plugins.RxJavaPlugins

class ShowsApplication : DaggerApplication() {

    override fun onCreate() {
        super.onCreate()
        RxJavaPlugins.setErrorHandler {
            //Fallback for undelivered exceptions
            if (it is UndeliverableException) {
                Log.w(applicationContext.packageName, "Undeliverable exception: ${it.cause}")
            }
        }
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> =
        DaggerShowsApplicationComponent.builder().create(this)


}