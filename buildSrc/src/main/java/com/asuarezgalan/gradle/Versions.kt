package com.asuarezgalan.gradle

object Versions {

    const val Kotlin = "1.3.41"
    const val Retrofit = "2.6.1"
    const val HttpLoggingInterceptor = "4.2.0"
    const val Dagger = "2.17"
    const val Glide = "4.9.0"
    const val LiveEvent = "1.0.1"

    object Android {
        const val Navigation = "2.1.0"
        const val ConstraintLayout = "1.1.3"
        const val LifecycleExtensions = "2.1.0"
        const val Core = "1.1.0"
        const val AppCompat = "1.1.0"
    }

    object Rx {
        const val RxKotlin = "2.4.0"
        const val RxAndroid = "2.1.1"
    }
}