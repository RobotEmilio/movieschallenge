package com.asuarezgalan.gradle

object Libs {

    const val Kotlin = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.Kotlin}"

    const val Glide = "com.github.bumptech.glide:glide:${Versions.Glide}"
    const val LiveEvent = "com.github.hadilq.liveevent:liveevent:${Versions.LiveEvent}"

    object Rx {
        const val RxKotlin = "io.reactivex.rxjava2:rxkotlin:${Versions.Rx.RxKotlin}"
        const val RxAndroid = "io.reactivex.rxjava2:rxandroid:${Versions.Rx.RxAndroid}"
    }

    object Retrofit {
        const val Retrofit = "com.squareup.retrofit2:retrofit:${Versions.Retrofit}"
        const val RetrofitGsonConverter = "com.squareup.retrofit2:converter-gson:${Versions.Retrofit}"
        const val HttpLoggingInterceptor = "com.squareup.okhttp3:logging-interceptor:${Versions.HttpLoggingInterceptor}"
        const val RetrofitRxAdapter = "com.squareup.retrofit2:adapter-rxjava2:${Versions.Retrofit}"
    }

    object Android {
        const val LifecycleExtensions =
            "androidx.lifecycle:lifecycle-extensions:${Versions.Android.LifecycleExtensions}"
        const val Core = "androidx.core:core-ktx:${Versions.Android.Core}"
        const val AppCompat = "androidx.appcompat:appcompat:${Versions.Android.AppCompat}"
        const val ConstraintLayout =
            "androidx.constraintlayout:constraintlayout:${Versions.Android.ConstraintLayout}"
        const val NavigationFragment =
            "androidx.navigation:navigation-fragment-ktx:${Versions.Android.Navigation}"
        const val NavigationUi =
            "androidx.navigation:navigation-ui-ktx:${Versions.Android.Navigation}"
    }

    object Dagger {
        const val Dagger = "com.google.dagger:dagger:${Versions.Dagger}"
        const val DaggerCompiler = "com.google.dagger:dagger-compiler:${Versions.Dagger}"
        const val DaggerAndroid = "com.google.dagger:dagger-android:${Versions.Dagger}"
        const val DaggerAndroidProcessor =
            "com.google.dagger:dagger-android-processor:${Versions.Dagger}"
        const val DaggerSupport = "com.google.dagger:dagger-android-support:${Versions.Dagger}"
    }
}