package com.asuarezgalan.gradle

object TestingLibs {

    const val JUnit = "junit:junit:${TestingVersions.JUnit}"

    object Android {
        const val TestRunner = "androidx.test:runner:${TestingVersions.Android.TestRunner}"
        const val Espresso = "androidx.test.espresso:espresso-core:${TestingVersions.Android.Espresso}"
    }

    const val MockK = "io.mockk:mockk:${TestingVersions.MockK}"
}