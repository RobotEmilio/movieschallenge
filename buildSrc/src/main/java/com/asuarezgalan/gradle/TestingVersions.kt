package com.asuarezgalan.gradle

object TestingVersions {
    const val JUnit = "4.12"

    const val MockK = "1.9.3"

    object Android {
        const val TestRunner = "1.2.0"
        const val Espresso = "3.2.0"
    }
}